<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 4/9/2019
 * Time: 2:40 PM
 */
?>

<div class="form-body">

    <div class="form-row">
        <label for="Your request is about" class="form-label hidden">Société</label>
        <div class="form-controls">[select* societe id:field-votre-demande-concerne class:select "Société" "Societe 1" "Societe 2" "Societe 3" "Societe 4" "Societe 5"]</div>
    </div>

    <div class="form-row">
        <label for="field-lname" class="form-label hidden">Last name*</label>
        <div class="form-controls">[text* lastname  id:field-nom class:field placeholder "Last name*"]</div>
    </div>

    <div class="form-row">
        <label for="field-fname" class="form-label hidden">First name*</label>
        <div class="form-controls">[text* firstname id:field-prenom class:field placeholder "First name*"]</div>
    </div>
    <div class="form-row">
        <label for="field-email" class="form-label hidden">E-mail*</label>
        <div class="form-controls">[email* email id:field-e-mail class:field placeholder "E-mail*"]</div>
    </div>
    <div class="form-row">
        <label for="field-nombre" class="form-label hidden">Nombre de participants*</label>
        <div class="form-controls">[text* nombre id:field-nombre class:field placeholder "Nombre de participants*"]</div>
    </div>




    <div class="cols">
        <div class="col col-1of2">
            <div class="form-row">
                <label for="field-nom" class="form-label hidden">Last name</label>
                <div class="form-controls">[text* lastname  id:field-nom class:field placeholder "Last name*"]</div>
            </div>
        </div>
        <div class="col col-1of2">
            <div class="form-row"><label for="field-prenom" class="form-label hidden">First name</label>
                <div class="form-controls">[text* firstname id:field-prenom class:field placeholder "First name*"]</div>
            </div>
        </div>
    </div>

    <div class="cols">
        <div class="col col-1of2"><div class="form-row"><label for="field-e-mail" class="form-label hidden">E-mail</label>
                <div class="form-controls">[email* email id:field-e-mail class:field placeholder "E-mail*"]</div>
            </div>
        </div>

        <div class="col col-1of2">
            <div class="form-row"><label for="field-telephone" class="form-label hidden">Phone number</label>
                <div class="form-controls">[text phone id:field-telephone class:field placeholder "Phone number"]</div>
            </div>
        </div>

    </div>

    <div class="form-row">
        <label for="field-message" class="form-label hidden">Message</label>
        <div class="form-controls">[textarea* message id:field-message class:field class:textarea placeholder "Message*"]</div>
    </div>
</div>
[recaptcha]
<div class="form-actions">[submit class:form-btn class:btn class:btn-white "Send my request"]</div>

