<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <?php if((is_page_template('templates/landing-page.php'))): ?><meta name=”robots” content=”noindex”> <?php endif; ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo TEMP_DIR_URI; ?>/css/images/favicon.ico" />
	<style>
		.category:hover .category-body {
			opacity: 1;
			cursor:url("<?php echo get_template_directory_uri(); ?>/images/cross.cur"), default;
		}
	</style>

   <?php wp_head(); ?>
</head> 
<body <?php body_class();?>>
<div class="wrapper">

    <?php if(is_page_template('templates/landing-page.php')): ?>
        <header class="header landing-header">
            <div class="shell">
                    <div class="header-inner">
                        <a href="<?php echo home_url();?>" class="brand-logo" ><image src="<?php echo  get_template_directory_uri(); ?>/images/logo.png" /></a>
                        <a href="<?php echo home_url();?>" class="brand-logo logo-secondary"><image src="<?php echo  get_template_directory_uri(); ?>/images/power-logo.jpg" /></a>
                    </div>
            </div><!-- /.shell -->
        </header><!-- /.header -->
    <?php else : ?>

    <header class="header">
        <div class="shell">
            <div class="bar">
                <nav class="nav-access">
                    <ul>
                            <li>
                                <a href="http://powerdata.ch/fr/login">
                                    <i class="ico-arrow"></i>
                                    <span class="text">Login Pro</span>
                                </a>
                            </li>
                    </ul>
                </nav><!-- /.nav-access -->

                <nav class="nav-lang">
                 <?php  $languages = icl_get_languages('skip_missing=1&orderby=id&order=asc&link_empty_to=str');
                                    //debug($languages);
                                    ?>
                    <ul>
                     <li> | </li>
                        <li><a href="http://rma.powerdata.ch/fr/rma" target="_blank"><?php echo __('RMA','powerdata');?>&nbsp;</li>
                            <?php if(!empty($languages)):
                                    foreach($languages as $language):
                                       // if($language['active']!=1):
                            ?>
                                 <li>
                                |
                            </li><li <?php echo $language['active']==1?'style="color:red"':'';?>> &nbsp;
                                    <a href="<?php echo $language['url']?>"><?php echo strtoupper($language['language_code'])?></a>
                                </li> 
                           <?php //endif;
                           endforeach;
                           endif;?>
                    </ul>
                </nav><!-- /.nav-lang -->
            </div><!-- /.bar -->

            <div class="header-inner">
                <a href="<?php echo home_url();?>" class="logo">Powerdata</a>

                <nav class="nav">
                <ul>
                <?php  wp_nav_menu(array(
                    'theme_location'=>'primary',
                    'items_wrap'=>'<li>%3$s</li>',
                    'container'=>false
                  
                    )
                );?>
                 <li>
                            <a href="javascript:;" class="btn-share" data-popover="popover-share">
                                <i class="ico-share"></i>
                            </a>

                                             <div class="popover" id="popover-share">
                                <ul class="list-share">
                                    <li>
                                        <a href="<?php echo get_field('facebook_link','option');?>" target="_blank">
                                            <i class="ico-facebook"></i>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo get_field('twitter_link','option');?>" target="_blank">
                                            <i class="ico-twitter"></i>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo get_field('linkedin_link','option');?>" target="_blank">
                                            <i class="ico-linkedin"></i>
                                        </a>
                                    </li>
                                </ul><!-- /.list-share -->
                        </div><!-- /.popover -->


                        </li>       

                     
                    </ul> 
                </nav><!-- /.nav -->
            </div><!-- /.header-inner -->

            
        </div><!-- /.shell -->

        <a href="javascript:;" class="btn-menu">
            <span></span>
            <span></span>
            <span></span>
        </a>
        <nav class="nav nav_share">
                <ul>

        <li class="share-bg">
                            <a href="javascript:;" class="btn-share mobile-btn-share" data-popover="popover-share">
                                <i class="ico-share"></i>
                            </a>

                                
                        <div class="popover" id="popover-share">
                                <ul class="list-share">
                                    <li>
                                        <a href="<?php echo get_field('facebook_link','option');?>" target="_blank">
                                            <i class="ico-facebook"></i>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo get_field('twitter_link','option');?>" target="_blank">
                                            <i class="ico-twitter"></i>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo get_field('linkedin_link','option');?>" target="_blank">
                                            <i class="ico-linkedin"></i>
                                        </a>
                                    </li>
                                </ul><!-- /.list-share -->
                         </div><!-- /.popover -->
                            

                        </li>

</ul>


        <div class="nav-mobile">
            <ul>

            <?php  wp_nav_menu(array(
                    'theme_location'=>'primary',
                    // 'items_wrap'=>'<li>%3$s</li>',
                    'container'=>false
                  
                    )
                );?>
               <!--  <li>
                    <a class="scroll-to" href="<?php echo home_url()?>#section-a-propos">About</a>
                </li>
                
              -->
            </ul>

              <ul>
                <li>
                    <a href="http://powerdata.ch/fr/login">Login Pro</a>
                </li>
            </ul>

             <ul>
            <?php if(!empty($languages)):
           // debug($languages);
                    foreach($languages as $language):

            ?>
                <li<?php echo $language['active']==1?'style="color:red"':'';?>>
                    <a href="<?php echo $language['url']?>"><?php echo strtoupper($language['language_code'])?></a>
                </li>
           <?php
           endforeach;

           endif;?>
            </ul>
        </div>
    </header><!-- /.header -->
    <?php endif; ?>