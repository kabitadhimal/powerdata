;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function() {
		$('.btn-menu').on('click', function() {
			$(this).toggleClass('btn-menu--open');
			$('.nav-mobile').toggleClass('visible');
		});

		// scroll to section
		

		$(".select").select2({
			minimumResultsForSearch:-1,
		});
	});

	$win.on('load', function() {
		$('.slider-testimonials .slides').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: true,
			arrows: false,
			responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});

		var wow = new WOW(
		  {
		    // boxClass:     'wow',      // animated element css class (default is wow)
		    // animateClass: 'animated', // animation css class (default is animated)
		    // offset:       0,          // distance to the element when triggering the animation (default is 0)
		    // mobile:       true,       // trigger animations on mobile devices (default is true)
		    // live:         true,       // act on asynchronously loaded content (default is true)
		    // scrollContainer: null // optional scroll container selector, otherwise use window
		  }
		);

		wow.init();
	});

	// POPOVERS
	$doc.on('click', '.btn-share', function(event) {
		var $target = $(this);

		if ( $target.hasClass('btn-share')) {
			event.preventDefault()

			var $popover = $('#' + $target.data('popover'))

			$popover.css({
				// top: event.pageY + 60 + 'px'
				// left: event.pageX - 40 + 'px'
			});

			$popover.addClass('open');

			$doc.mouseup(function(event) {
				if (!$popover.is(event.target) && $popover.has(event.target).length === 0)
				{
					$popover.removeClass('open');
				}
			});
		};
	});

	$doc.on("scroll", function(){
		if ($(document).scrollTop() > 0){
			$(".nav-mobile").addClass("fixed");
		} else {
			$(".nav-mobile").removeClass("fixed");
		}
	});	

	
	
	

	
})(jQuery, window, document);

// masonry for testimonials
$(window).on('load resize', function() {
  if ($('.section-body').length) {
	$('.section-body')
	.css('max-width', $(window).width());
  }
});
$(window).on('load', function() {
  if ($('.section-body').length) {
	setTimeout(function() {
	  $('.team-testimonials').masonry({
		columnWidth: 0,
		itemSelector: '.testimonial'
	  });
	}, 1);
  }
});
