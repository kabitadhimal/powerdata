<?php

function add_post_type($name, $args = array()) {
    add_action('init', function() use($name, $args) {
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));

        $args = array_merge(
                array(
            'public' => true,
            'label' => "$upper",
            'labels' => array('add_new_item' => "Add new $upper"),
            'supports' => array('title', 'editor', 'thumbnail')
                ), $args);

        register_post_type($name, $args);
    });
}

/*
 * Taxonomy
 */

function add_taxonomy($name, $post_type, $args = array()) {
    $name = strtolower($name);
    add_action('init', function() use($name, $post_type, $args) {
        $args = array_merge(
                array(
            'label' => ucwords($name),
            'show_ui' => true,
            'query_var' => true,
                ), $args);
     
        register_taxonomy($name, $post_type, $args);
    });
}

/*Account creation related function*/

function build_email_template($parse_data = '', $type = '') {
    $template = file_get_contents_curl(EBUM_TEMPLATE_URL . $type . '.html');
    $email_content = strtr($template, $parse_data);
    return $email_content;
}

function build_email($parse_data='', $template='') {
    $email_content = strtr($template, $parse_data);
    return $email_content;
}

function generate_user_guid($length = 16) {
    /* generate random string */
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $guid = '';
    for ($i = 0; $i < $length; $i++) {
        $guid .= $characters[rand(0, $charactersLength - 1)];
    }

    global $wpdb;
    $meta_key = 'guid';
    $sql = "SELECT count(*) FROM " . $wpdb->users . " as u
            JOIN " . $wpdb->usermeta . " as um "
            . "ON u.ID=um.user_id AND um.meta_key='" . $meta_key . "' "
            . "WHERE um.meta_value='" . $guid . "'";
    $count = $wpdb->get_var($sql);

    if ($count > 0) {
        generate_user_guid($length);
    }

    return $guid;
}

function check_valid_guid($guid = '') {
    global $wpdb;
    $meta_key = 'guid';
    $sql = "SELECT count(*) FROM " . $wpdb->users . " as u
            JOIN " . $wpdb->usermeta . " as um "
            . "ON u.ID=um.user_id AND um.meta_key='" . $meta_key . "' "
            . "WHERE um.meta_value='" . $guid . "'";
    $count = $wpdb->get_var($sql);
    return $count > 0 ? true : false;
}

function email_exists_check($email) {
    if ($user = get_user_by('email', $email)) {
        $current_user = wp_get_current_user();
        if ($user->user_email == $current_user->user_email)
            return false;
        else
            return $user->ID;
    }

    return false;
}

//called funciton



