<?php get_header(); ?>


    <?php
    $blocks = get_field('contents', 'option');
    //debug($blocks);
    if ($blocks) {
        foreach ($blocks as $block):

            $tpl = __DIR__ . '/partial/flex-content/' . strtolower($block['acf_fc_layout']) . '.php';

            include $tpl;
        endforeach;
    }
    ?>  

<?php get_footer(); ?>
