<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iam
 */

get_header(); ?>
<?php if (have_posts()) : 

  while (have_posts()) : the_post(); 
          $id = get_the_ID();
          ?>
<!-- NGL news details title banner -->
                    <section class="ipro-banner ipro-banner--details">
                        <div class="ipro-container ipro-container--main">
                        
                            <!-- Back to news details -->
                            <div class="ipro-banner__link ipro-banner__link--newsDetails">
                             <a href="javascript:;" onclick="goBack()" class="ipro-link ipro-link--underline ipro-link--stomGrey"><em><?php echo _e('Return','ngl')?></em></a>    
                              
                            </div><!-- /.#Back to news details block -->

                            <!-- NGL news details banner title -->
                            <div class="ipro-banner__title text-center">
                                <h2><?php echo the_title()?></h2>
                            </div><!-- /.#NGL news details banner title block -->

                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL news details title banner -->

                    <!-- NGL News details post content block -->
                    <section class="ipro-block ipro-block--single ipro-block--post">
                        <div class="ipro-container ipro-container--main">

                            <div class="ipro-post ipro-post--single">
                                <!-- Post image -->
                                <figure class="ipro-post__thumbnail">
                                   <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($id));?>
                                    <img src="<?php echo $image_url?>" class="ipro-post__img ipro-valign--middle" alt="Post detail image" />
                                </figure><!-- /.#Post image -->

                                <!-- Post body -->
                                <div class="ipro-post__body">
                                    <p><?php the_content();?></p>
                                </div><!-- /.#Post body -->
                            </div>

                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL News details post content block -->
                <?php 
                        $events_slug_arr = array('events-zh-hans','events-de','events-it','events-es','events','events-2');
                       
                        $categories = get_the_category( $id );
                        if(!empty($categories)):
                             $category = $categories[0]->slug;
                             if(in_array($category,$events_slug_arr)):
                                
                          
                 ?>
                    <!-- NGL News details post form block -->
                                <section class="ipro-block ipro-block--form">
                                    <div class="ipro-container ipro-container--main">
                                    
                                        <!-- NGL post form -->
                                         <div class="ipro-form-row text-center">
                                            <h3><?php echo __('MEET US','ngl')?></h3>

                                            <div class="ipro-form__wrap"> 
                                                <div class="ipro-form ipro-form--single" action="" method="post">


                                                  <?php echo do_shortcode('[contact-form-7 id="254" title="Event Post Form"]');?>
                                                   
                                                </div>

                                            </div>
                                        </div> 
                                        <!-- NGL post form -->
                                        
                                    </div><!-- /.# NGL main container -->
                                </section>
                <?php       endif;
                endif;
                ?>











	
<?php endwhile;
endif;?>


<script type="text/javascript">
    function goBack() {
    window.history.back();
}
</script>
<?php



get_footer();
