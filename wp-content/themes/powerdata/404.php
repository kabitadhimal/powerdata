<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package iam
 */

get_header(); ?>
    <section class="section-testimonials inner-page">
        <div class="section-head">
            <div class="shell"> <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'powerdata' ); ?></h1></div>
        </div>
        <div class="section-body">
            <div class="shell" style="text-align: center">
            <p><?php esc_html_e( 'It looks like nothing was found at this location. Please try browsing another page ?', 'powerdata' ); ?></p>
            </div>

        </div><!-- .page-content -->
    </section>
<?php
get_footer();
