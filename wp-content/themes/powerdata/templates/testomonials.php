<?php /*Template Name: Testomonials Template */?>
<?php get_header();?>


    
    <section class="section-testimonials inner-page">
        <div class="shell">
            <a href="javascript:;" onclick="goBack()" class="btn"><i class="arrow-active arrow-active--dir__left"></i><?php _e('Back','powerdata'); ?>
            </a>
        </div>  
        <div class="section-head">
            <h2 class="section-title"><?php _e('What people say about us','powerdata'); ?></h2><!-- /.section-title -->
        </div><!-- /.section-head -->

        <?php 
        
        $posts_per_page='-1';
        $args = array(
            'post_type' =>'testimonial',
            'posts_per_page'=>$posts_per_page,
            'status'=>'published',
            'orderby'=>'title',
            'order'=>'ASC'
             );
        $query= new WP_Query($args);
        ?>

        <div class="section-body">
            <div class="shell">
                <div class="team-testimonials" id="testimonial-outer"> 

                <?php if($query->have_posts()): while($query->have_posts()): $query->the_post();
                    $ids[] = get_the_ID();
                ?>

                   <div class="testimonial wow fadeInUp">
                        <div class="testimonial-image">
                        
                        <?php  $image_url = get_the_post_thumbnail_url(); if($image_url!=''):?>
                            <img src="<?php echo $image_url;?>" alt="" />
                         <?php endif;?>   
                        </div><!-- /.testimonial-image -->

                        <div class="testimonial-head">
                            <h5><?php the_title();?></h5>

                            <h6><?php the_field('contact_name');?></h6>
                        </div><!-- /.testimonial-head -->

                        <div class="testimonial-body">
                            <p><?php the_content();?></p>
                        </div><!-- /.testimonial-body -->
                    </div><!-- /.testimonial -->

                <?php endwhile; 
                      endif;
                      wp_reset_postdata();
                ?>
                  
                 
                </div><!-- /.slider-testimonials -->
            </div><!-- /.shell -->
        </div><!-- /.section-body -->

 
 
    </section><!-- /.section-testimonials -->

   



<script type="text/javascript">
    function goBack() {
            window.history.back();
    }
    jQuery(document).ready(function ($) {
        

        function initialiceMasonry(){
            var $container = $('#testimonial-outer');        
                $container.imagesLoaded(function() {               
                    $container.masonry("reloadItems");
                
                    $container.masonry({
                        isInitLayout : true,
                        itemSelector: '.testimonial'
                    });
                });
    }

    function resizes(){
        if ($('.section-body').length) {
        $('.section-body')
        .css('max-width', $(window).width());
            }
    }

    function maintain() {
        if ($('.section-body').length) {
        setTimeout(function() {
//          $('.team-testimonials').masonry({
//            columnWidth: 0,
//            itemSelector: '.testimonial'
//          });
         }, 1);
        }
    }

       
      
    });
</script>
<?php get_footer();?>