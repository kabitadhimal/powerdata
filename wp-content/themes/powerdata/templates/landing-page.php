<?php /*Template Name: Landing Template */
get_header();
?>
<section class="section-contacts" id="section-contact">
    <div class="shell">
        <div class="cols">
            <div class="col col-1of1">
                <div class="form-contact">
                    <?php $contact_form_title = get_field('contact_form_title','option'); ?>
                    <?php
                     $cf = get_field('select_form');
                     $content = get_the_content();
                     if(!empty($content)):
                    ?>
                    <div class="section-content"><?php the_content(); ?></div>
                    <?php endif; ?>
                    <?php
                    if(!empty($cf)):
                        echo do_shortcode('[contact-form-7 id="'.$cf.'" title="Landing page Form"]');
                    endif;
                    ?>
                </div><!-- /.form-contact -->
            </div><!-- /.col col-1of2 -->

        </div><!-- /.cols -->
    </div><!-- /.shell -->
</section><!-- /.section-contacts -->
<?php get_footer();