<?php /*Template Name: Brands(Marques) Template */ ?>
<?php get_header();
while (have_posts()):the_post();
    ?>
    <section class="section-testimonials inner-page">
        <div class="shell">
            <a href="javascript:;" onclick="goBack()" class="btn"><i
                        class="arrow-active arrow-active--dir__left"></i><?php _e('Back', 'powerdata'); ?>
            </a>
        </div>
        <div class="section-head">
            <h2 class="section-title"><?php the_title() ?></h2><!-- /.section-title -->
        </div><!-- /.section-head -->

        <div class="section-body">
            <div class="shell">
                <div class="marques__group">
                    <?php $brand_logos = get_field('brand_logos');
                    if (!empty($brand_logos)):
                        foreach ($brand_logos as $brand_logo):?>
                            <div class="marques__list wow fadeInUp">
                                <?php if (!empty($brand_logo['link'])): ?>
                                    <a href="<?php echo $brand_logo['link'] ?>"
                                       title="<?php echo $brand_logo['title'] ?>" target="_blank">
                                        <img src="<?php echo $brand_logo['logo']['url'] ?>"
                                             alt="<?php echo $brand_logo['logo']['alt'] ?>"/>
                                    </a>
                                <?php else:
                                    ?>
                                    <img src="<?php echo $brand_logo['logo']['url'] ?>"
                                         alt="<?php echo $brand_logo['logo']['alt'] ?>"/>

                                <?php endif; ?>
                            </div>
                        <?php endforeach;
                    endif; ?>


                </div>
            </div><!-- /.shell -->
        </div><!-- /.section-body -->

        <div class="section-actions clear-both">
            <?php if (ICL_LANGUAGE_CODE == 'en'):
                $link = 'http://powerdata.ch/en/2-m?alternate=0';
            else:
                $link = 'http://powerdata.ch/fr/2-m?alternate=0';
            endif; ?>
            <a class="btn" href="<?php echo $link ?>"
               target="_blank"><?php echo __('View our catalogue', 'powerdata') ?></a>
        </div><!-- /.section-actions -->
    </section><!-- /.section-testimonials -->

<?php endwhile ?>

<?php get_footer(); ?>
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }


</script>