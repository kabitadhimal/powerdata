<?php /*Template Name: Home Template */ ?>

<?php get_header();

while (have_posts()): the_post();
    $content = get_field('contents');

//debug($content);
    if (!empty($content)):
        foreach ($content as $block):?>

            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'header_section'): ?>
                <div class="intro">
                    <div class="intro-bg">
                        <script type="text/javascript">
                            video = "<?php echo $block['choose_video']['url']?>";
                        </script>

                        <video id="video_play" width="100%" height="100%" src="<?php echo $block['choose_video']['url']?>" playsinline="playsinline" autoplay="autoplay" loop="loop"
                               muted="muted"></video>
                    </div><!-- /.intro-bg -->

                    <div class="intro-inner">
                        <div class="shell">
                            <div class="intro-content">
                                <h1 class="intro-title"><?php echo $block['title'] ?> </h1><!-- /.intro-title -->
                                <br style="clear: both"/>
                                <p><?php echo $block['sub_title'] ?></p>

                                <div class="intro-actions">
                                    <a href="#section-a-propos" class="btn about_us"><?php echo $block['button_text'] ?></a>
                                </div><!-- /.intro-actions -->
                            </div><!-- /.intro-content -->
                        </div><!-- /.shell -->
                    </div><!-- /.intro-inner -->

                    <div class="home-overlay-content"></div>
                </div><!-- /.intro -->

            <?php endif; ?>

            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'text_zone_section'): ?>
                <section class="section-message" id="section-a-propos">
                    <div class="shell">
                        <div class="section-inner">
                            <div class="section-head">
                                <h2 class="section-title">
                                    <?php echo $block['title'] ?>
                                </h2><!-- /.section-title -->
                            </div><!-- /.section-head -->

                            <div class="section-body">
                                <?php echo $block['text'] ?>

                                <div class="section-actions">
                                    <a href="<?php echo $block['link'] ?>" target="_blank" class="link">
                                        <i class="arrow-active"></i>

                                        <span class="text"><?php echo $block['link_text'] ?></span>
                                    </a>
                                </div><!-- /.section-actions -->
                            </div><!-- /.section-body -->
                        </div><!-- /.section-inner -->
                    </div><!-- /.shell -->
                </section><!-- /.section-message scroll-to -->
            <?php endif; ?>


            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'services_section'): ?>
                <section class="section-services" id="section-services">
        <span class="number zoomIn wow">
            <span><?php echo $block['number_of_services'] ?></span>
        </span>

                    <div class="section-head">
                        <h2 class="section-title"><?php echo $block['title'] ?></h2><!-- /.section-title -->
                    </div><!-- /.section-head -->

                    <div class="section-body">
                        <div class="grid grid-2-cols">
                            <ul>
                                <?php if (!empty($block['service_blocks'])):
                                    foreach ($block['service_blocks'] as $service):?>
                                        <li class="grid-item">
                                            <div class="service <?php echo $service['is_image_right_side'] == 1 ? 'service-reversed' : '' ?>">
                                                <div class="service-image"
                                                     style="background-image: url(<?php echo $service['image']['url'] ?>); "></div>
                                                <!-- /.service-image -->

                                                <div class="service-body">
                                                    <h5 class="service-title"><?php echo $service['title_'] ?></h5>
                                                    <!-- /.service-title -->

                                                    <p><?php echo $service['text'] ?></p>
                                                </div><!-- /.service-body -->
                                            </div><!-- /.service -->
                                        </li><!-- /.grid-item -->
                                    <?php endforeach;
                                endif; ?>


                            </ul>
                        </div><!-- /.grid -->
                    </div><!-- /.section-body -->
                </section><!-- /.section-services scroll-to -->
            <?php endif; ?>
            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'distribution_channels_section'): ?>
                <section class="section-features" id="section-canaux">
        <span class="number zoomIn wow">
            <span><?php echo $block['no_of_distribution_channels'] ?></span>
        </span>

                    <div class="section-bg"
                         style="background-image: url(<?php echo TEMP_DIR_URI ?>/css/images/features-bg.jpg); "></div>
                    <!-- /.section-bg -->

                    <div class="section-inner">
                        <div class="section-head">

                            <h2 class="section-title"><?php echo $block['title'] ?></h2><!-- /.section-title -->
                        </div><!-- /.section-head -->

                        <div class="section-body">
                            <div class="shell">
                                <ul class="list-features">
                                    <?php if (!empty($block['pictograms'])):
                                        foreach ($block['pictograms'] as $pictogram):?>
                                            <li>
                                                <span class="icon-block"><i class="<?php echo $pictogram['picture'] ?>"></i></span>

                                                <h5><?php echo $pictogram['text'] ?></h5>
                                            </li>
                                        <?php endforeach;
                                    endif; ?>


                                </ul><!-- /.list-features -->
                            </div><!-- /.shell -->
                        </div><!-- /.section-body -->
                    </div><!-- /.section-inner -->
                </section><!-- /.section-features -->
            <?php endif; ?>
            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'products_section'): ?>
                <section class="section-cegories" id="section-produits">
        <span class="number zoomIn wow">
            <span><?php echo $block['number_of_products'] ?></span>
        </span>

                    <div class="section-head">
                        <h2 class="section-title"><?php echo $block['title'] ?></h2><!-- /.section-title -->
                    </div><!-- /.section-head -->

                    <div class="section-body">
                        <div class="grid grid-3-cols">
                            <ul>
                                <?php if (!empty($block['products'])):
                                    foreach ($block['products'] as $product):
                                        ?>
                                        <li class="grid-item">
                                            <a href="<?php echo $product['link'] ?>" target="_blank">
                                                <div class="category">
                                                    <div class="category-image"
                                                         style="background-image: url(<?php echo $product['image']['url'] ?>); "></div>
                                                    <!-- /.category-image -->

                                                    <div class="category-body">
                                                        <h4><?php echo $product['text'] ?></h4>
                                                    </div><!-- /.category-body -->

                                                </div><!-- /.category -->
                                            </a>
                                        </li><!-- /.grid-item -->

                                    <?php endforeach;

                                endif; ?>


                            </ul>
                        </div><!-- /.grid -->
                    </div><!-- /.section-body -->

                    <div class="section-actions">
                        <a href="<?php echo $block['left_button_link'] ?>"
                           class="btn" target="_blank"><?php echo $block['left_button_text'] ?></a>

                        <a href="<?php echo $block['right_button_link'] ?>"
                           class="btn"><?php echo $block['right_button_text'] ?></a>
                    </div><!-- /.section-actions -->
                </section><!-- /.section-cegories -->
            <?php endif; ?>
            <?php if (!empty($block['acf_fc_layout']) && $block['acf_fc_layout'] == 'partners_section'): ?>

                <section class="section-testimonials">
                    <div class="section-head">
                        <h2 class="section-title"><?php echo $block['title'] ?></h2><!-- /.section-title -->
                    </div><!-- /.section-head -->

                    <div class="section-body">
                        <div class="shell">
                            <div class="slider-testimonials">
                                <div class="slider-clip">
                                    <div class="slides">
                                        <?php
                                        $query = new WP_Query(
                                                array(
                                                        'post_type' => 'testimonial',
                                                         'posts_per_page'=>9,
                                                            'orderby' => 'rand',
                                                            )
                                                        );
                                        while($query->have_posts()):$query->the_post();

                                       // if (!empty($block['testinonial_slider'])):
                                            //foreach ($block['testinonial_slider'] as $ind => $testimonial):
                                                ?>
                                                <div class="slide">
                                                    <div class="testimonial">
                                                        <div class="testimonial-image">
                                                            <?php $image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
                                                            <img src="<?php echo $image_url; ?>" alt=""/>
                                                        </div><!-- /.testimonial-image -->

                                                        <div class="testimonial-head">
                                                            <h5><?php echo get_the_title(); ?></h5>

                                                            <h6><?php echo get_field('contact_name',get_the_ID()) ?></h6>
                                                        </div><!-- /.testimonial-head -->

                                                        <div class="testimonial-body">
                                                            <?php
                                                                $content = get_the_content();
                                                            if (!empty($content)): ?>
                                                                <span data-short_desc="<?php echo wp_trim_words($content, 20, ''); ?>"
                                                                      data-long_desc="<?php echo $content ?>"
                                                                      class="testimonial-text"
                                                                      style="display:none"></span>
                                                                <p class="testimonial-body-text"><?php echo wp_trim_words($content, 25, ''); ?></p>
                                                                <a class="more" href="javascript:;">...</a>

                                                            <?php endif; ?>


                                                        </div><!-- /.testimonial-body -->
                                                    </div><!-- /.testimonial -->
                                                </div><!-- /.slide -->
                                                <?php
                                                endwhile;
                                            //endforeach;
                                       // endif ?>


                                    </div><!-- /.slides -->
                                </div><!-- /.slider-clip -->
                            </div><!-- /.slider-testimonials -->
                        </div><!-- /.shell -->
                    </div><!-- /.section-body -->

                    <div class="section-actions">
                        <a class="btn"
                           href="<?php echo home_url('/testimonial'); ?>"><?php echo __('View all testimonials', 'powerdata') ?></a>
                    </div><!-- /.section-actions -->
                </section><!-- /.section-testimonials -->
            <?php endif; ?>


            <?php

        endforeach;
    endif;
endwhile;
?>


<script>
    $(function () {

        // $('#video_play').attr('src', video);

        $(window).on('load', function () {
            if($(window).width() > 767){
                if ($("#video_play").length > 0){
                    $("#video_play")[0].play();
                }
            }
        });


        $('.more').click(function (e) {
            e.stopPropagation();
            var readmore_text = $(this).parent('.testimonial-body').find('.testimonial-text').data('long_desc');
            $(this).siblings('p.testimonial-body-text').text(readmore_text);
            $(this).hide();
        });



        $('.scroll-to').on('click', function (event) {
            event.preventDefault();
            target = $(this).children('a').attr('href');

            var hash = target.substring(target.indexOf('#'));
            $target = $(hash);
            var mainOffset = $target.offset().top;
            var ActualOffset = mainOffset - 20 - $('.header').height();

            $('html, body').stop().animate({
                'scrollTop': ActualOffset
            }, 900);
        });

        $('.about_us').on('click', function (event) {
            event.preventDefault();
            target = $(this).attr('href');

            var hash = target.substring(target.indexOf('#'));

            $target = $(hash);
            var mainOffset = $target.offset().top;
            var ActualOffset = mainOffset - 20 - $('.header').height();

            $('html, body').stop().animate({
                'scrollTop': ActualOffset
            }, 900);
        });



        $(window).load(function () {
            var hash = window.location.hash
            if (hash) {
                $target = $(hash);

                if ($target.length != 0) {
                    var mainOffset = $target.offset().top;
                    var ActualOffset = mainOffset - 20 - $('.header').height();

                    $('html, body').stop().animate({
                        'scrollTop': 0
                    }, 900);
                }
            }

        });




    });
</script>
<?php get_footer(); ?>
