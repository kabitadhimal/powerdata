
<?php get_header(); ?>

   <?php while(have_posts()): the_post(); ?>
			<?php    $blocks = get_field('contents');
					//debug($blocks);

			if(!empty($blocks)):
				foreach ($blocks as $block) :
					   $tpl = __DIR__.'/partial/flex-content/'.strtolower($block['acf_fc_layout']).'.php';
						include $tpl;
				endforeach;

			endif;

			 ?>

   <?php endwhile; ?>

 
<?php get_footer(); ?>

