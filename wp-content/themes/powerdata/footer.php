<?php if(!(is_page_template('templates/landing-page.php'))): ?>
<section class="section-contacts" id="section-contact">
        <div class="shell">
            <div class="cols">
                <div class="col col-1of2">
                    <div class="form-contact">
                       <?php $contact_form_title = get_field('contact_form_title','option'); ?>
                       <?php $contact_form = get_field('contact_form','option'); ?>
                            <div class="form-head">
                                <h2 class="section-title"><?php echo $contact_form_title; ?></h2><!-- /.form-title -->
                            </div><!-- /.form-head -->
                            <?php echo do_shortcode($contact_form);?>

                    </div><!-- /.form-contact -->
                </div><!-- /.col col-1of2 -->

                <div class="col col-1of2">
                    <div class="section-body">
                    <?php $title = get_field('title','option');
                     $text = get_field('text','option');
                     $button_text = get_field('button_text','option');
                     $button_url = get_field('button_url','option');

                    ?>
                        <h2 class="section-title"><?php echo $title?></h2><!-- /.section-title -->

                        <div class="section-content">
                            <p><?php echo $text?></p>
                        </div><!-- /.section-content -->
                        <div class="section-actions">
                            <a href="<?php echo $button_url?>" class="btn btn-white" target="_blank"><?php echo $button_text?></a>
                        </div><!-- /.section-actions -->
                    </div><!-- /.section-body -->
                </div><!-- /.col col-1of2 -->
            </div><!-- /.cols -->
        </div><!-- /.shell -->
    </section><!-- /.section-contacts -->
<?php endif; ?>


<?php if(is_page_template('templates/landing-page.php')): ?>
    <footer class="footer landing-footer">
        <div class="shell">
            <div class="">
                <div class="footer-aside">
                    <span>&copy;<?php echo date('Y')?> PowerData SA</span>
                </div><!-- /.footer-aside -->
            </div><!-- /.footer-inner -->
        </div><!-- /.shell -->
    </footer><!-- /.footer -->
    <?php else : ?><footer class="footer">
        <div class="shell">
            <div class="footer-inner">
                <div class="footer-aside">
                    <a href="<?php echo home_url()?>" class="logo-footer"></a>
                </div><!-- /.footer-aside -->

                <div class="footer-content">
                    <div class="footer-cols">
                        <div class="footer-col">
                            <h6> <?php echo get_field('first_column_title','option'); ?></h6>

                            <nav class="nav-footer">
                                <ul>
                                <?php $sitemaps= get_field('sitemap','option');
                                if(!empty($sitemaps)):
                                    foreach($sitemaps as $ind=>$sitemap):?>
                                    <li class="<?php echo $ind==4?'':'scroll-to'?>">
                                        <a href="<?php echo $sitemap['link']?>"><?php echo $sitemap['name']?></a>
                                    </li>
                                <?php
                                    endforeach;
                                endif;?>


                                </ul>
                            </nav><!-- /.nav-footer -->
                        </div><!-- /.footer-col -->

                        <div class="footer-col">
                            <h6><?php echo get_field('second_column_title','option'); ?></h6>

                            <p><?php echo get_field('second_column_text','option'); ?> </p>
                        </div><!-- /.footer-col -->

                        <div class="footer-col">
                            <h6><?php echo get_field('3rd_column_title','option'); ?></h6>

                            <p>
                               <?php echo get_field('3rd_column_text','option'); ?>
                            </p>
                        </div><!-- /.footer-col -->
                    </div><!-- /.footer-cols -->
                </div><!-- /.footer-content -->
            </div><!-- /.footer-inner -->

            <div class="footer-bar">
                <div class="socials">
                    <ul>
                        <li>
                            <a href="<?php echo get_field('facebook_link','option');?>" target="_blank">
                                <i class="ico-facebook"></i>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo get_field('twitter_link','option');?>" target="_blank">
                                <i class="ico-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo get_field('linkedin_link','option');?>" target="_blank">
                                <i class="ico-linkedin"></i>
                            </a>
                        </li>

                        <li class="scroll-to">
                            <a href="<?php echo home_url()?>#section-contact" >
                                <i class="ico-envelope"></i>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.socials -->

                <div class="copyright">
                    <span>&copy;<?php echo date('Y')?> PowerData SA</span>  <span>|</span>  <span>Design by <a href="https://www.procab.ch/" target="_blank">procab</a></span>
                </div><!-- /.copyright -->
            </div><!-- /.footer-bar -->
        </div><!-- /.shell -->
    </footer><!-- /.footer -->
<?php endif; ?>
</div><!-- /.wrapper -->
<script>
    $(function(){

            $( window ).scroll(function() {
                 $( ".header" ).addClass('sticky');
            });

            $('.mobile-btn-share').click(function(){
                $(this).parent().children('.popover').toggleClass("open");
            });

            $( 'p:empty' ).remove();

        $('.scroll-to > a[href="#section-contact"]').on('click', function (event) {
            event.preventDefault();
            var mainOffset = $('#section-contact').offset().top;
            
            var ActualOffset = mainOffset - 20 - $('.header').height();
            $('html, body').stop().animate({
                'scrollTop': ActualOffset
            }, 1000);
        });


    });


</script>
<?php wp_footer(); ?>
</body>
</html>
