<?php
define('TEMP_DIR_URI', get_template_directory_uri());
define('TEMP_DIR', get_template_directory());
define('SITEURL', site_url());
define('HOMEURL', home_url());
require TEMP_DIR . '/functions/function-helpers.php';
/**
 * ngl functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ngl
 */
if (!function_exists('debug')) {


    function debug($var = '', $die = true) {
        $array = debug_backtrace();
        echo '<br/>Debugging from ' . $array[0]['file'] . ' line: ' . $array[0]['line'];

        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if ($die !== FALSE) {
            die();
        }
    }

}

remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 12);




if (!function_exists('powerdata_setup')) :

    function powerdata_setup() {

        load_theme_textdomain('ngl', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');

        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'ngl'),
            'footer_menu' =>'Footer Menu'
        ));
        

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'author',
            'gallery',
            'status',
            'audio',
        ));
    }

endif;
add_action('after_setup_theme', 'powerdata_setup');

function powerdata_content_width() {
    $GLOBALS['content_width'] = apply_filters('powerdata_content_width', 640);
}

add_action('after_setup_theme', 'powerdata_content_width', 0);

function powerdata_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'ngl'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'powerdata_widgets_init');



/* ================================================================================
  ENQUEUE SCRIPTS AND STYLES.
  ================================================================================== */

function powerdata_scripts() {


     wp_enqueue_style('animate-style', TEMP_DIR_URI . '/js/vendor/animate.css');
     wp_enqueue_style('slick-style', TEMP_DIR_URI . '/js/vendor/slick/slick.css');
     wp_enqueue_style('select2-style', TEMP_DIR_URI . '/js/vendor/select2/select2.min.css');
     
     
    wp_enqueue_style('ngl-style', get_stylesheet_uri());
    wp_enqueue_style('main-style', TEMP_DIR_URI . '/css/style.css');
    wp_enqueue_style('custom-style', TEMP_DIR_URI . '/css/custom.css','','1.1','');

    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', TEMP_DIR_URI . "/js/vendor/jquery-1.12.4.min.js", false, null);
        wp_enqueue_script('jquery');
    }
    wp_enqueue_script('wow-jquery', TEMP_DIR_URI . '/js/vendor/wow.min.js', array('jquery'), '1.0');
    wp_enqueue_script('easing-jquery', TEMP_DIR_URI . '/js/jquery.easing.1.3.js', array('jquery'), '1.0');
    wp_enqueue_script('slick-jquery', TEMP_DIR_URI . '/js/vendor/slick/slick.min.js', array('jquery'), '1.0');
    // if (is_page_template( 'templates/news.php' )):   
    // endif;
    wp_enqueue_script('select2-jquery', TEMP_DIR_URI . '/js/vendor/select2/select2.min.js', array('jquery'), '1.0');  
    wp_enqueue_script('masonry-jquery', TEMP_DIR_URI . '/js/vendor/masonry/js/masonry.pkgd.min.js', array('jquery'), '1.0');
    wp_enqueue_script('meanmenu', TEMP_DIR_URI . '/scss/vendor/meanmenu/js/jquery.meanmenu.min.js', array('jquery'), '1.0');
    wp_enqueue_script('imagesloaded-jquery', TEMP_DIR_URI . '/js/vendor/masonry/js/imagesLoaded.min.js', array('jquery'), '1.0');  
    //wp_enqueue_script('readmore-jquery', TEMP_DIR_URI . '/js/vendor/readmore.min.js', array('jquery'), '1.0');
    wp_enqueue_script('functions-jquery', TEMP_DIR_URI . '/js/functions.js', array('jquery'), '1.0');
			/*Include only in home page */
       // if (is_front_page()):       
       // 	endif;
}



add_action('wp_enqueue_scripts', 'powerdata_scripts');


remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 12);



/* ================================================================================
  REMOVE ILLEGAL CHARACTERS FROM FILES DURING UPLOADING ON WORDPRESS
  ================================================================================== */
add_filter('wp_handle_upload_prefilter', 'custom_upload_filter', 1, 1);

function custom_upload_filter($file) {
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $file['name']);
    return $file;
}

/* ================================================================================
  CREATE THE OPTIONS PAGE
  ================================================================================== */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Footer Options',
        'menu_title' => 'Footer Options'
    ));
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Options',
        'menu_title' => 'Options'
    ));
}







/* =========================================================
  Add custome taxonomy
  ======================================================== */





/* =========================================================
  Add custom post type
  ======================================================== */

add_post_type('testimonial', array(
    'public' => true,
    'menu_icon' => 'dashicons-calendar',
    'label' => "All Testimonial",
    'labels' => array(
        'add_new_item' => "Add new testimonial"
    ),
    'supports' => array(
        'title',
        'editor',
        'thumbnail'
       
    )
));







/* ================================================================================
  Function which remove Plugin Update Notices – nav-menu-images
  ================================================================================== */



add_filter('site_transient_update_plugins', 'disable_plugin_updates');

function disable_plugin_updates($value) {
    //debug($value);
    if (!empty($value))
        unset($value->response['nav-menu-images/nav-menu-images.php']);
    return $value;
}





/* ================================================================================
  Function to remove pingback header.
  ================================================================================== */


add_filter('wp_headers', 'remove_x_pingback_header');

function remove_x_pingback_header($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}

add_filter('xmlrpc_enabled', '__return_false');

//
/* ================================================================================
  Removes the meta tag generator name and version that generated with the wp_head()function.
  ================================================================================== */
remove_action('wp_head', 'wp_generator');

function completely_remove_wp_version() {
    return ''; //returns nothing, exactly the point.
}

add_filter('the_generator', 'completely_remove_wp_version');

function remove_generator_from_rss2($gen, $type) {
    return '';
}

add_filter("get_the_generator_rss2", "remove_generator_from_rss2", 10, 2);



/*Map Co-ordinates Management from backend*/


add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
   
    
    if ( is_page_template( 'templates/brands.php' ) ) {
        $classes[] = 'brands-page';
    }

    if ( is_page_template( 'templates/home-page.php' ) ) {
        $classes[] = 'home-page';
    }

    if ( is_page_template( 'templates/testomonials.php' ) ) {
        $classes[] = 'testimonial-page';
    }      

    
    return $classes;

}

/*
 * Modify Login
 */

function procab_login_logo() {
    echo '<style type="text/css">
        @import('.get_bloginfo( 'template_directory' ) .'/assets/fonts/admin-login-css/style.css);
        
        body{
            background: #fff;
        }
        .login h1 a { background-image:url('. get_bloginfo( 'template_directory' ) .'/images/logo.png) !important;background-size: 100% !important; margin-bottom: 20px; height: 55px; width: 205px; }
        .login h1 a:hover,
        .login h1 a:focus {
            box-shadow: none;
            outline: 0;
        }
        #loginform {
            background: #eb3f33;            
        }

        #loginform .button-primary {
            background-color: #76777a;
            box-shadow: none;
            transform: none;
            text-shadow: none;
        }
        #loginform .button-primary:focus,
        #loginform .button-primary:active, {
        #loginform .button-primary:active:focus
            box-shadow: none;
            transform: none;
        }
        .login form .input, .login input[type=text], input[type=password] {
            display: block;
            margin-top: 10px;
            padding: 10px 20px;
            background: #eb3f33;
            border: 1px solid #ffffff;
            color: #ffffff;
            font-size: 16px;            
        }
        .login form .input:focus, .login input[type=text]:focus, input[type=password]:focus {
            outline: 0;
            box-shadow: none;
        }
        .login label {        
            color: #ffffff;
            font-size: 16px;
        }
        input[type=text]:focus, input[type=password]:focus {
            outline: 0;
        }
        input:-webkit-autofill {
            -webkit-text-fill-color: #ffffff !important;
            -webkit-box-shadow: 0 0 0 1000px #eb3f33 inset !important;
        }
        input::-webkit-input-placeholder { / Chrome/Opera/Safari /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input::-moz-placeholder { / Firefox 19+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-ms-input-placeholder { / IE 10+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-moz-placeholder { / Firefox 18- /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }

        .login a {
            color: #393939;
            transition: all linear 0.35s;
        }

        .login #nav a ,
        .login #backtoblog a {
            color: #393939;
            font-size: 16px;
            font-weight: 600;
        }

        .login #backtoblog a {
            color: #eb3f33;
        }

        .login #nav a:hover,
        .login #nav a:focus,
        .login #backtoblog a:hover,
        .login #backtoblog a:focus {
            color: #eb3f33;
            box-shadow: none;
            outline: 0;
        }

        .login form .forgetmenot label {
            font-size: 14px;
        }

    </style>';
}
add_action('login_head', 'procab_login_logo');



