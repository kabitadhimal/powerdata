<?php 
$menu = wp_get_menu_array('Main menu');
//debug($menu);

?>
                        <div class="col-sm-10 col-xs-12 ipro-valign--middle ipro-navigation--col">

                                    <!-- NGL and Supports Brand and toggle get grouped for better mobile display -->
                                    <div class="ipro-navbar-header">
                                        <a href="" class="ipro-navbar__toggler closer" data-collapse="ipro-nav--mobile">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </a>
                                    </div><!-- /.#NGL and Supports Brand and toggle get grouped for better mobile display -->
                                      <nav class="ipro-nav ipro-nav--mobile" role="navigation">

                                      <!-- NGL header navigation left list group col -->
                                        <ul class="ipro-menu--main-group" role="menu">
                                        <?php if(!empty($menu)): 
                                            foreach($menu as $m):
                                                if(!empty($m['classes']) && in_array('has-megamenu--style1',$m['classes'])):
                                                    ?>
                                                     <li class="has-megamenu has-megamenu--style1" data-target="ipro-menu__dropdown">
                                                         <a href="<?php echo $m['url']?>"><?php echo $m['title']?></a>
                                                         <!-- NGL mega menu style1 -->
                                                                <div class="ipro-menu__submenu ipro-menu__dropdown">

                                                                    <div class="ipro-container ipro-container--main">
                                                                        <!-- NGL mega menu col group row -->
                                                                        <div class="row clearfix ipro-row  ipro-flex">
                                                                            <?php if(!empty($m['children'])):
                                                                            $child_arr = array();
                                                                                foreach($m['children'] as $ind=>$child):
                                                                                    $child_arr['childd'][$ind] = $child;
                                                                                endforeach;

                                                                               //debug($child_arr);
                                                                            ?>
                                                                            <!-- NGL mega menu left col -->
                                                                            <div class="col-sm-6 col-xs-12 ipro-flex__col">
                                                                                <div class="ipro-col__content">

                                                                                    <ul class="ipro-menu__submenu-list">
                                                                                     <?php $i=0;
                                                                                            foreach($child_arr['childd'] as $child):
                                                                                                 if($i>4)
                                                                                                     break;                                                                                                 
                                                                                                ?>
                                                                                                 <li><a href="<?php echo $child['url']?>"><img src="<?php echo $child['image']?>" alt="" class="ipro-valign--middle"><span><?php echo $child['title']?></span></a></li>
                                                                                            <?php  
                                                                                            $i++;
                                                                                            endforeach; ?>
                                                                                       
                                                                                    </ul>

                                                                                </div><!-- /.#NGL container -->
                                                                            </div><!-- /.#NGL mega menu left col -->


                                                                            <!-- NGL mega menu right col -->
                                                                            <div class="col-sm-6 col-xs-12  ipro-flex__col">
                                                                                <div class="ipro-col__content">
                                                                                        
                                                                                    <ul class="ipro-menu__submenu-list">
                                                                                     <?php $j=0;
                                                                                            foreach($child_arr['childd'] as $child):
                                                                                                $j++;
                                                                                                if($j<6)
                                                                                                  continue;
                                                                                                     if(!empty($child['classes']) && !in_array('logo-group',$child['classes'])):
                                                                                             
                                                                                                ?>
                                                                                                    <li><a href="<?php echo $child['url']?>"><img src="<?php echo $child['image']?>" alt="" class="ipro-valign--middle"><span><?php echo $child['title']?></span></a> </li>
                                                                                             <?php 
                                                                                                     endif;
                                                                                            endforeach;?>
                                                                                      
                                                                                    </ul>

                                                                                    <!-- NGL - Mega menu footer logo group -->

                                                                                    <ul class="ipro__submenu__logo-group clearfix">
                                                                                        <?php   foreach($child_arr['childd'] as $child):
                                                                                                     if(!empty($child['classes']) && in_array('logo-group',$child['classes'])):                                          
                                                                                                        ?>
                                                                                                        <li><a href="<?php echo $child['url']?>"><img src="<?php echo $child['image']?>" alt="" class="ipro-valign--middle"></a> </li>
                                                                                                        <?php 
                                                                                                      endif;
                                                                                              endforeach;?>

                                                                                        
                                                                                    </ul><!-- /.#NGL - Mega menu footer logo group -->

                                                                                </div><!-- /.#NGL container -->
                                                                            </div><!-- /.#NGL mega menu right col -->
                                                                            <?php endif;?>
                                                                        </div><!-- /.#NGL mega menu col group row -->
                                                                    </div>
                                                                </div><!-- /.#NGL mega menu style1 -->
                                                    </li>
                                                <?php
                                                elseif(!empty($m['classes']) && in_array('has-megamenu--style2',$m['classes'])): 
                                                   if(!empty($m['children'])):
                                                    ?>
                                                 <li class="has-megamenu has-megamenu--style2" data-target="ipro-menu__dropdown">
                                                         <a href="<?php echo $m['url']?>"><?php echo $m['title']?></a>
                                                    <div class="ipro-menu__submenu ipro-menu__dropdown">

                                                    <div class="ipro-container ipro-container--main">
                                                        <!-- NGL mega menu col group row -->
                                                        <div class="row clearfix ipro-row  ipro-flex">
                                                                <?php //debug($m['children']);?>
                                                            <!-- NGL mega menu left col -->
                                                            <?php foreach($m['children'] as $childMenu): 
                                                                        //debug($childMenu);
                                                                        //
                                                            ?>
                                                            <div class="col-sm-6 col-xs-12 ipro-flex__col">
                                                                <div class="ipro-col__content">

                                                                    <div class="row row--submenu clearfix">
                                                                        <div class="col-sm-3 col-xs-12 ipro-menu__submenu__col">
                                                                            <figure>
                                                                                <a href="<?php echo $childMenu['url']?>"><img src="<?php echo $childMenu['image']?>" alt="" class="ipro-valign--middle" /></a>
                                                                            </figure>
                                                                        </div>
                                                                        <div class="col-sm-9 col-xs-12 ipro-menu__submenu__col">
                                                                            <h3><a href="<?php echo $childMenu['url']?>"><?php echo $childMenu['title']?></a></h3>
                                                                            <p><?php echo $childMenu['description']?></p>
                                                                        </div>
                                                                    </div>

                                                                </div><!-- /.#NGL container -->
                                                            </div>
                                                        <?php endforeach ?>
                                                            <!-- NGL mega menu right col -->
                                                           

                                                        </div><!-- /.#NGL mega menu col group row -->
                                                    </div>
                                                </div>
</li>
     <?php endif;?>                                         
                                                <?php else:
                                                    ?>
                                                        <li><a href="<?php echo $m['url']?>"><?php echo $m['title']?></a></li>
                                                <?php endif;
                                            endforeach;

                                         endif;?>
                                           
                                        </ul><!-- /.#NGL header navigation left list group col -->
                                    </nav><!-- /.#NGL mobile nav -->


                           
                                </div><!-- /.#NGL navigation logo col -->
           
      

 